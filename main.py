# -*- coding: utf-8 -*-
import json
import os

from datetime import datetime

from kivy.app import App

from kivy.properties import DictProperty, StringProperty, NumericProperty, ListProperty

from kivy.uix.button import Button
from kivy.uix.image import AsyncImage as Image
from kivy.uix.label import Label
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.scrollview import ScrollView
from kivy.clock import Clock
from kivy.metrics import dp

# KivyMD stuff
from kivymd.theming import ThemeManager
from kivymd.label import MDLabel
from kivymd.list import OneLineAvatarListItem, ILeftBodyTouch
from kivymd.list import MDList
from kivymd.button import MDRaisedButton, MDFlatButton
from kivymd.toolbar import Toolbar
from kivymd.dialog import MDDialog
import kivymd.snackbar as Snackbar


class ItemButton(Button):
    name = StringProperty('')
    price = NumericProperty('')
    img = StringProperty('')

    def __init__(self, **kwargs):
        super(ItemButton, self).__init__(**kwargs)

    def get_height(self, *largs):
        self.height = self.texture.height / 3

class RecieptButton(MDFlatButton):
    name = StringProperty('')
    count = NumericProperty('')
    price = NumericProperty('')

    def __init__(self, **kwargs):
        super(RecieptButton, self).__init__(**kwargs)


class Reciept_Dropdown(MDDialog):
    pass


class RootCalc(FloatLayout):

    items = DictProperty({
        'brus': ('brus', 25, './img/brus.png'),
        'kaffe': ('kaffe', 10, './img/kaffe.png'),
        'vaffel': ('vaffel', 15, './img/vaffel.png'),
        'polse': ('polse', 20, './img/polse.png'),
        'kake': ('kake', 15, './img/kake.png'),
        'pizza': ('pizza', 30, './img/pizza.png'),
        '2pizzabrus': ('2 pizza 1 brus', 70, './img/pizzabrus.png')
    })
    total_sum = NumericProperty(0)
    total_price = ListProperty([])
    reciept = DictProperty({})

    def __init__(self, **kwargs):
        super(RootCalc, self).__init__(**kwargs)
        Clock.schedule_once(self.populate_list, 1)

    def populate_list(self, *largs):
        for item, value in self.items.iteritems():

            listeitem = ItemButton(name=value[0],
                        price=value[1],
                        background_normal=value[2])
            listeitem.bind(on_release=self.add_item)
            self.ids.itemscrollist.add_widget(listeitem)

    def add_item(self, instance):
        # Check if the key exists
        if not self.reciept.has_key(instance.name):
            self.reciept[instance.name] = [0, 0]
        self.reciept[instance.name][0] += 1
        self.reciept[instance.name][1] += int(instance.price)

        self.update_price()

    def remove_item(self, instance):
        if self.reciept[instance.name][0] == 0:
            return
        count_before = self.reciept[instance.name][0]
        self.reciept[instance.name][0] = self.reciept[instance.name][0] - 1
        self.reciept[instance.name][1] = (self.reciept[instance.name][1] /
                                          count_before *
                                          self.reciept[instance.name][0])
        instance.text = '{} {}'.format(instance.name,
                                       self.reciept[instance.name][0])
        self.update_price()

    def update_price(self):
        self.total_sum = sum([x[1] for x in self.reciept.values()])

    def open_dropdown(self, instance):
        scroll = ScrollView()
        content = MDList()
        for i in self.reciept:
            if self.reciept[i][0] == 0:
                break
            item = RecieptButton(text='{} {}'.format(i, self.reciept[i][0]),
                                theme_text_color='Secondary',
                                name=i,
                                count=self.reciept[i][0],
                                total_price=self.reciept[i][1])
            item.bind(on_release=self.remove_item)
            content.add_widget(item)
        self.remove_dialog = Reciept_Dropdown(title='Ta bort',
                         size_hint=(.8,.8))
        self.remove_dialog.add_widget(content)
        self.remove_dialog.add_action_button('Ferdig',
                                 action=lambda*x: self.remove_dialog.dismiss())
        self.remove_dialog.open(instance)

    def reset(self):
        self.total_sum = 0
        self.write_reciept()
        self.reciept = {}

    def write_reciept(self):
        data_dir = App.get_running_app().data_dir
        print data_dir
        if not os.path.isfile(data_dir + '/kiosk_calculator.json'):
            with open(os.path.join(data_dir, 'kiosk_calculator.json'), 'w+') as f:
                print os.path.join(data_dir, "kiosk_calculator.json")
                print "tried to write ospathisfile"
                f.write('{}')
        with open(data_dir + '/kiosk_calculator.json', 'r') as f:
            print "reading"
            data = json.loads(f.read())

        time_ident = datetime.now().strftime('%Y-%m-%d-%H-%M-%S')
        data[time_ident] = self.reciept

        with open(data_dir + '/kiosk_calculator.json', 'w') as f:
            print "writing"
            f.write(json.dumps(data))


class CalculatorApp(App):

    theme_cls = ThemeManager()

    def build(self):
        root_widget = RootCalc()
        self.data_dir = getattr(self, "user_data_dir")
        self.theme_cls.theme_style = "Dark"
        self.theme_cls.primary_palette = "Green"
        self.theme_cls.primary_hue = "900"

        return root_widget

    def on_pause(self):
        return True


if __name__ == '__main__':
    CalculatorApp().run()
